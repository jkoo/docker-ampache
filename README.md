# Ampache Music Server on Alpine Linux Docker

The image contains several components:

* Alpine Linux, a lightweight Linux distrubtion
* Ampache, a music server
* MySQL, a database used by Ampache
* PHP, a front-end used by Ampache
* Apache, a web service used by Ampache

## Usage

```sh
docker create --name ampache \
  -v /path-to/music:/music \
  -p 80:80 \
  kukki/docker-ampache
docker start ampache
```

__Note__

* You would like to change the port `80` to a different port number if you're using an existing web server on you host.

## Setup

* MySQL admin password can be retreived by running following command. Or you can specify the admin password by adding `-e MYSQL_PASS=MyPassword` in `docker create` options

```ps
docker logs ampache
```

Ampache setup is quite straight-forward:

* Visit `http://localhost/ampache`
* Pick up a desired language
* Enter `127.0.0.1` in MySQL server host
  * Note: Apache and MySQL are running in the same docker
* Enter `admin` and admin password or `root` and empty password as MySQL credential
* It's a good idea to create a MySQL account for Ampache. So check the last checkbox at the bottom
* Click `Next` and finish the rest as desred
* Enjoy your music

## Issue and questions

* Any issues or suggestions, please [report here](https://bitbucket.org/jkoo/docker-ampache/issues?status=new&status=open)

## License

* [GNU General Public License v3](http://www.gnu.org/licenses/gpl-3.0.en.html)

