#!/bin/sh

VOLUME_HOME="/var/lib/mysql"

chown -R mysql:mysql $VOLUME_HOME

if [[ ! -d $VOLUME_HOME/mysql ]]; then
    echo "=> An empty or uninitialized MySQL volume is detected in $VOLUME_HOME"
    echo "=> Installing MySQL ..."
    mysql_install_db > /dev/null 2>&1
    echo "=> Done!"
    /create_mysql_admin_user.sh
else
    echo "=> Using an existing volume of MySQL"
fi

# Start apache in the background
apachectl start

# Start cron in the background
crond

# Start a process to watch for changes in the library with inotify
(
while true; do
    inotifywatch /media
    php /var/www/localhost/ampache/bin/catalog_update.inc -a
    sleep 30
done
) &

# run this in the foreground so Docker won't exit
exec mysqld_safe
